//
//  StatusBarView.swift
//  PdfTranslator
//
//  Created by Viktor Kushnerov on 9/15/19.
//  Copyright © 2019 Viktor Kushnerov. All rights reserved.
//

import SwiftUI
import Combine

struct StatusBarView: View {
    @EnvironmentObject var store: Store
    
    var body: some View {
        HStack() {
            StatusBarView_ViewMode().padding(5)
            StatusBarView_PdfPage()
            Divider().fixedSize()
            StatusBarView_Tabs(viewMode: $store.viewMode, currentTab: $store.currentTab)
            #if os(macOS)
            #else
            StatusBarView_Zoom()
            #endif
            if store.viewMode == .web { Divider().fixedSize() }
            StatusBarView_Voice().padding([.top,.bottom], 5)
        }.padding(.trailing, 20)
    }
}

struct StatusBarView_Previews: PreviewProvider {
    static var previews: some View {
        StatusBarView().environmentObject(Store.shared)
    }
}
